import React, { Component } from 'react';
import request from 'superagent';

class App extends Component{

	constructor(){
		super();
		this.state = {
			users: []
		}
	}
	
	componentWillMount(){
		request
			.get('http://localhost:8080/api/users')
			.end((err, res) => {
				const users = JSON.parse(res.text).users;
				this.setState({
				users: users
				});
			});

	}

	render(){
		var users =  this.state.users.map((user, i) => {
		return <li key={i}>{user.nombre}</li>
		});
		
		}
}

export default App;