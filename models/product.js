

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchemas = Schemas({
	
	id:String,
	name: String,
	description: String,
	image:String,
	price:String

})

mongoose.model('Product', ProductSchemas);