const express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var multer = require('multer');
var cloudinary = require("cloudinary");
var methodOverride = require('method-override');

cloudinary.config({
	cloud_name:"dbwzcnogi",
	api_key:"423297384771934",
	api_secret:"qDgzw00sr8l0GMt-ICKHE7HViQ4"

});

const app = express();


app.set("view engine","jade");
app.use(express.static('public'));

mongoose.connect('mongodb://localhost:27015/Carrito', (err, res) => {
	if (err) throw err
		console.log('Conexion a la base de datos establecida ');

	app.listen(8080, function(){	
	console.log('server on port 8080');
	});
}); 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
var uploader = multer({dest: "./uploads"}); 
var middleware_upload = uploader.single('image_avatar');
app.use(methodOverride('_method'));
//app.use(multer({dest: "./uploads"}).single('image_avatar'));

 
//Se define el schema del producto
  var productSchema = {
    title:String,
    description:String,
    imageUrl:String,
    price:Number,
    cantidad:Number
  };

var Product = mongoose.model("Product", productSchema);
app.get("/",function(req,res){
	res.render("index");
});

app.get("/menu",function(req,res){
 	Product.find(function(error,documento){
		if (error) {console.log(error);}
		res.render("menu/index",{ products:documento })
	});
});	

// Se edita el producto
app.put("/menu/:id",function(req,res){		
	console.log(data);

	cantidad =0;
	cantidad: req.body.cantidad ++;			
	var data = {
		title: req.body.title,
		description: req.body.description,
		price: req.body.price,
		cantidad: req.body.cantidad
	};	
	console.log(data);
	Product.update({"_id": req.params.id}, data, function(product){
		res.redirect("/menu");
	});
});

//Eliminar producto
app.get("/menu/delete/:id",function(req,res){
	var id = req.params.id;
	Product.findOne({"_id": id }, function(err, producto){
		res.render("menu/delete",{producto: producto}); 
	});
});

app.delete("/menu/:id",function(req,res){
	var id = req.params.id
	Product.remove({"_id": id },function(err){
		if(err){console.log(err)}
		res.redirect("/menu");
	});
});

app.get("/menu/edit/:id",function(req,res){
	var id_producto = req.params.id;
	console.log(id_producto);
	Product.findOne({"_id": id_producto},function(error,producto){		
		res.render("menu/edit",{product: producto });
	});
});

//se listan los productos
app.get("/admin/lista",function(req,res){
	Product.find(function(error,documento){
		res.render("admin/lista",{ products: documento })
	});
});
	
app.post("/menu",middleware_upload,function(req, res){	
	//Se define el schema del producto
	console.log(req.body);
	var data = {
		title: req.body.title,
		description: req.body.description,
		imagenUrl: "data.png",
		price: req.body.price,
		cantidad: req.body.cantidad
	}

	 var product = new Product(data);

	if(req.file){ 
	cloudinary.uploader.upload(req.file.path, //Aqui se quita eel name y se pone file y el path 
		function(result) 
		{ product.imageUrl = result.url; 
			product.save(function(err){
			console.log(product);
			console.log(req.file);
			res.render("index"); 
	 	 }); 
		}); 
	}
 
});
//direccionamiento cuenta
app.get("/menu/cuenta",function(req,res){
	Product.find(function(error,documento){
		res.render("menu/cuenta",{ products: documento })

	});
});

app.get("/menu/new",function(req,res){
	res.render("menu/new");
});
